1. Wszystkie kotki, które jako jedyne wygraly jakikolwiek certyfikat.
(Istnieje taki certyfikat, że ten nasz kotek go ma i nie istnieje kotek inny od naszego kotka,
który miałby ten certyfikat.
Istnieje takie nadanie certyfikatu, że ten nasz kotek jest tam wpisany i nie istnieje kotek inny od naszego kotka,
dla ktorego istnialby wpis w nadaniu certyfikatu z tym samym id certyfikatu.)



SELECT imie, id
FROM KOTKI kotek
where exists(
	SELECT id
  	from nadane_certyfikaty
  	where kotek.id = nadane_certyfikaty.id_kotka
  		and not exists (
        	select id
          	from kotki kx
          	where kx.id <> kotek.id
          		AND EXISTS (
                	select id
                  	from nadane_certyfikaty ndx
                  	where ndx.id_kotka = kx.id and ndx.id_certyfikatu = nadane_certyfikaty.id_certyfikatu
                )
        )
)



2. Wszyscy uzytkownicy, którzy mają jakiegoś kotka rasy czarnej.
(Taki użytkownik, że istnieje kotek, który ma rase czarna, który to kotek jest posiadany przez tego użytkownika.)


select imie, id
from uzytkownicy
where EXISTS (
  select kotki.id
  from kotki join rasa on rasa.id = kotki.rasa_id
    join wlasnosc_kotka on wlasnosc_kotka.id_kotka = kotki.id
  where rasa.OPIS_RASY = 'czarny' and wlasnosc_kotka.id_nabywcy = uzytkownicy.id
  ) ;


3. Ile każda rasa ma naliczone certyfikatów (chodzi o każdy dyplom każdego kotka tej rasy).

Select rasa.opis_rasy, count(*)
from nadane_certyfikaty join kotki on nadane_certyfikaty.id_kotka = kotki.id
	join rasa on rasa.id = kotki.rasa_id
group by rasa.opis_rasy


4. Dla każdego certyfikatu: jego nazwę i ilość dyplomów z niego

SELECT nazwa_certyfikatu,
COUNT(*)
FROM
	certyfikaty_kotka
join nadane_certyfikaty
on certyfikaty_kotka.id = nadane_certyfikaty.id_certyfikatu
GROUP BY
	certyfikaty_kotka.nazwa_certyfikatu;


5. W jednym zapytaniu wszystkie rasy bez kotków oraz wszystkie kotki bez rasy.

select opis_rasy, kotki.imie
from rasa full OUTER join kotki on rasa.id = kotki.rasa_id
where rasa.opis_rasy is null or kotki.imie is NULL;


6. Kotki z rasą, które nie mają żadnych certyfikatów.

select opis_rasy, kotki.imie
from rasa join kotki on rasa.id = kotki.rasa_id
left join nadane_certyfikaty on kotki.id = nadane_certyfikaty.id_kotka
where nadane_certyfikaty.id_kotka is null;

7. Wszystkie rasy, które mają co najmniej dwa kotki.

select opis_rasy from rasa
inner join kotki on rasa.id = kotki.rasa_id
group by rasa.id
HAVING COUNT (kotki.rasa_id) >= 2;



