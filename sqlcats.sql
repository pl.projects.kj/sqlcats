CREATE TABLE RASA (ID BIGINT PRIMARY KEY, OPIS_RASY TEXT NOT NULL);

INSERT INTO RASA VALUES (1, 'bialy'), (2, 'rudy'), (3, 'kremowy'), (4, 'zielony'), (5, 'czarny'), (6, 'różowe');;



CREATE TABLE kotki (ID bigINT PRIMARY KEY, rasa_id bigint references rasa(id), imie text not null,  srednia_cena_w_zl double precision check(srednia_cena_w_zl > 0));

INSERT INTO kotki (id, rasa_id, imie, srednia_cena_w_zl)
VALUES
	   (71, 1 , 'Tomek' , 10) ,
       (72, 5 , 'Ola' , 20) ,
       (733, 2 , 'Mirek' , 1) ,
       (73, 2 , 'Mirek' , 3) ,
       (74, 3 , 'Lila' , 25) ,
       (754, 4 , 'Aleksander' , 20),
       (75, 5 , 'Aleksandero' , 20),
       (76, 1 ,  'Pimpuś', 35),
       (77, null ,  'Franciszek ', 2137);



CREATE TABLE uzytkownicy (ID bigint PRIMARY KEY, Imie TEXT not null, Pesel text UNIQUE check(length(pesel) = 11) not null, Miasto Text);

INSERT INTO uzytkownicy (id, imie, pesel, miasto)
VALUES
	   (1, 'Pelagiusz' , '12312312312', 'Wroclaw'),
       (2, 'Marcjanna' , '12318312312', 'Warszawa'),
       (3, 'Jozef' , '12312712312',  'Wroclaw'),
       (4, 'Apolonia' , '12352312312', 'Wroclaw'),
       (5, 'Gwidon' , '12316312312',  'Bialystok'),
       (6, 'Alan' , '12316315552',  'Bełchatów');



create table wlasnosc_kotka (id_kotka bigint primary key, id_nabywcy bigint not null);

alter table wlasnosc_kotka add CONSTRAINT fk_kotekposiadany FOREIGN KEY (id_kotka)
 REFERENCES kotki(id);
alter table wlasnosc_kotka add CONSTRAINT fk_wlascicielposiadajacy FOREIGN KEY (id_nabywcy)
 REFERENCES uzytkownicy(id);

insert into wlasnosc_kotka (id_kotka, id_nabywcy)
VALUES (71, 1),
       (72, 1),
       (733, 2),
       (73, 3),
       (74, 4),
       (754, 4),
       (75, 5),
       (76, 5);



CREATE TABLE CERTYFIKATY_KOTKA (ID bigINT PRIMARY KEY, nazwa_certyfikatu TEXT not null,
skrot_nazwy_instytucji text not NULL check(length(skrot_nazwy_instytucji) < 5));

insert into certyfikaty_kotka (id, nazwa_certyfikatu, skrot_nazwy_instytucji)
VALUES
	   (1, 'Food without getting dirty', 'POQ'),
       (2, 'Clean litter box', 'POQ'),
       (3, 'Fragrant fur', 'ELO'),
       (4, 'Not a troublemaker', 'HWDP'),
       (5, 'Feline friend', 'WEW');



create table nadane_certyfikaty (id_kotka bigint not null, id_certyfikatu bigint not null);
alter table nadane_certyfikaty add CONSTRAINT fk_kotekicertfikaty FOREIGN KEY (id_kotka)
 REFERENCES kotki(id);
alter table nadane_certyfikaty add CONSTRAINT fk_certyfikatyikotek FOREIGN KEY (id_certyfikatu)
 REFERENCES certyfikaty_kotka(id);

 INSERT INTO nadane_certyfikaty (id_kotka, id_certyfikatu)
 VALUES
	   (71, 1),
       (72, 1),
       (733, 2),
       (73, 3),
       (74, 4),
       (754, 4),
       (75, 5);
